# README #

This repository is the code of keyword search.

### What is this repository contained? ###

* 1.preprocessing
* 2.get inverted index
* 3.extract main idea
* 4.cosine similarity
* 5.build kmeans model

### The intermediate result ###

* 1.After preprocessing, we will get 4 json document:
		output_article_index, output_article_words,output_title_index, output_article_counts
* 2.We use output_article_words to get inverted index, this step we will get 1 json document:
		inverted_index
* 3.We use output_article_words and output_title_index to extract main idea, this step we will get 1 json document:
		one_article_tfidf
* 4.We use one_article_tfidf,inverted_index,output_article_index to get cosine similarity.
* 5.For kmeans, we use all words output_all_words to build model,this step we will get 2 files:
		kmeans.pkl, output_term.json
* 6.Run kmeans, we use the model get the cluster file: cluster.json

