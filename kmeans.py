'''
Author: Xinjie LU  z5101488
this file is used to generate kmeans model (kmeans.pkl) and all term (output_term.json)
the model is based on the tfidf matrix
'''
import os
import nltk
from bs4 import BeautifulSoup as bs
import string
from nltk.corpus import stopwords 
from nltk.stem.wordnet import WordNetLemmatizer
import re
import json
import operator
from sklearn import feature_extraction
from sklearn.feature_extraction.text import TfidfTransformer  
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer 
from sklearn.cluster import KMeans
from sklearn.externals import joblib

# to calculate tfidf, we need join the string together
def transform_real_words(real_words):
    transformed_words = []
    string = ' '.join(real_words)
    transformed_words.append(string)
    return transformed_words

# build tfidf matrix and get all the term
def calculate_tfidf(words):
    corpus = words  
    transformer = TfidfVectorizer()
    tfidf = transformer.fit_transform(corpus)
    term = transformer.get_feature_names()
    return tfidf,term

#the matrix we get： doc1， doc2， doc3
##              term1
##              term2
##              term3


# build kmeans model
def kmeans(matrix):
    num_clusters = 10
    km = KMeans(n_clusters = num_clusters)
    km.fit(matrix)
    joblib.dump(km, 'kmeans.pkl')
    return None

def submain():
    f = open('./output_all_words.json').read()
    real_words = json.loads(f)
    all_words = []
    for (k,v) in real_words.items():
        trans_words = transform_real_words(v)
        for content in trans_words:
            all_words.append(content)

    tfidf,term= calculate_tfidf(all_words)
    kmeans(tfidf)

    feature = json.dumps(term,indent=4)

    output_feature = open('./output_term.json', 'w')
    output_feature.write(feature)
    output_feature.close()


submain()
        

    
    
        
    

    
