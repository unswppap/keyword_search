'''
Author: Xinjie LU  z5101488
this file is used to get clusters : 
the class number and article index in each class (cluster.json)
'''
import json
from sklearn.externals import joblib

def submain():
    pair = {}
    f = open('./output_term.json')
    term = json.load(f)
    km = joblib.load('./kmeans.pkl')
    clusters = km.labels_.tolist()

    for i in range(len(clusters)):
        l = []
        if clusters[i] not in pair:
            l.append(i)
            pair[clusters[i]] = l
        else:
            pair[clusters[i]].append(i)

    order_centroids = km.cluster_centers_.argsort()[:, ::-1]
    for i in range(10):
        print("Cluster %d words:" % i, end='')
        for ind in order_centroids[i, :50]:
            print(term[ind])

    cluster = json.dumps(pair, indent=4)

    output_cluster = open('./cluster.json', 'w')
    output_cluster.write(cluster)
    output_cluster.close()

submain()
