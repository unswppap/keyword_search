'''
Author: Xinjie LU  z5101488
this file used to get main purpose of each article
after runing this file ,we can get one dictionary like this:
{article1: [(word1, weight1),(word2, weight2)...]} 
for each article, the items is sorted by their tfidf value
'''

import os
import nltk
from bs4 import BeautifulSoup as bs
import string
from nltk.corpus import stopwords 
from nltk.stem.wordnet import WordNetLemmatizer
import re
import json

import operator
from sklearn import feature_extraction
from sklearn.feature_extraction.text import TfidfTransformer  
from sklearn.feature_extraction.text import CountVectorizer 


# to calculate tfidf, we need join the string together
def transform_real_words(real_words):
    transformed_words = []
    string = ' '.join(real_words)
    transformed_words.append(string)
    return transformed_words

#calculate tfidf for items in each article, if the item is appeared in the title,
#then we give it a weight > 1
def calculate_tfidf(words,title):
    corpus = words  
    vectorizer = CountVectorizer() 
    transformer = TfidfTransformer()
    tfidf = transformer.fit_transform(vectorizer.fit_transform(corpus))
    word = vectorizer.get_feature_names()
    weight = tfidf.toarray()

    tfidf = {}
    sorted_tfidf = {}
    for i in range(len(weight)):
        for j in range(len(word)):
            if word[j] in title:                
                tfidf[word[j]] = weight[i][j] * 1.2
            else:
                tfidf[word[j]] = weight[i][j]
    temp = sorted(tfidf.items(), key = operator.itemgetter(1),reverse = True)
    for pairwise in temp:
        sorted_tfidf[pairwise[0]] = float("%.8f" % pairwise[1])
    return sorted_tfidf


def get_one_tfidf(real_words,article_title):
    one_article_tfidf = {}
    for (k ,v) in real_words.items():
        title = article_title[k] #
        print(k)
        if v = []:
            continue

        trans_words = transform_real_words(v)
        one_tfidf = calculate_tfidf(trans_words,title)
        one_article_tfidf[k] = one_tfidf
    return one_article_tfidf

# write the generated file
def write_json(a):
    one_tfidf_json = json.dumps(a,indent = 4)
    output_one_article_tfidf = open('./one_article_tfidf.json', 'w')
    output_one_article_tfidf.write(one_tfidf_json)
    output_one_article_tfidf.close()

def submain():
    f_words = open('./output_article_words.json').read()
    f_title = open('./output_title_index.json').read()
    real_words = json.loads(f_words)
    article_title = json.loads(f_title)
    one_article_tfidf = get_one_tfidf(real_words,article_title)
    write_json(one_article_tfidf)

        
submain()
