'''
Author: Xinjie LU  z5101488
this file used to get inverted index
after runing this file ,we can get one dictionary like this:
{word1:[(article1,weight1),(article2, weight2)]} 
for each word, the article is sorted by their tfidf value
'''
import os
import nltk
from bs4 import BeautifulSoup as bs
import string
from nltk.corpus import stopwords 
from nltk.stem.wordnet import WordNetLemmatizer
import re
import json
import operator
from sklearn import feature_extraction
from sklearn.feature_extraction.text import TfidfTransformer  
from sklearn.feature_extraction.text import CountVectorizer 



### to calculate tfidf, we need join the string together
def transform_real_words(real_words):
    transformed_words = []
    string = ' '.join(real_words)
    transformed_words.append(string)
    return transformed_words


#calculate tfidf for each word
def calculate_tfidf(words):
    corpus = words  
    vectorizer = CountVectorizer() 
    transformer = TfidfTransformer()
    tfidf = transformer.fit_transform(vectorizer.fit_transform(corpus))
    word = vectorizer.get_feature_names()
    weight = tfidf.toarray()

    tfidf = {}
    sorted_tfidf = {}
    for j in range(len(word)): 
        tfidf = {}
        for i in range(len(weight)):
            if weight[i][j] == 0.0:
                pass
            else:
                tfidf[i] = float("%.8f" % weight[i][j])
        temp = sorted(tfidf.items(), key=operator.itemgetter(1), reverse=True)
        sorted_tfidf[word[j]] = temp
    return sorted_tfidf
    
def get_tfidf():
    all_words = []
    for (k,v) in real_words.items():
        trans_words = transform_real_words(v)
        for content in trans_words:
            all_words.append(content)

    word_inverted_index = calculate_tfidf(all_words)
    return word_inverted_index

# write the generated file
def write_json(a):
    inverted_index_json = json.dumps(a,indent = 4)
    output_inverted_index = open('./inverted_index.json', 'w')
    output_inverted_index.write(inverted_index_json)
    output_inverted_index.close()
    
def submain():
    f = open('./output_article_words.json').read()
    real_words = json.loads(f)
    word_inverted_index = get_tfidf()
    print('calculate done!')
    write_json(word_inverted_index)

submain()
    
    
